package edu.westga.cs1302.university.model;

/**
 * Defines a student
 * 
 * @author CS 1302
 * @version Spring 2023
 */
public class Student {
	private String name;
	private int grade;

	/**
	 * Creates a new Student with the specified name and grade.
	 * 
	 * @precondition name!=null AND !name.isEmpty() AND grade >=0 AND grade <= 100
	 * @postcondition getName() == name AND getGrade() == grade
	 * 
	 * @param name  name of the student
	 * @param grade the students numeric grade (between 0 and 100, inclusive)
	 */
	public Student(String name, int grade) {
		if (name == null) {
			throw new IllegalArgumentException("name cannot be null.");
		}

		if (name.isEmpty()) {
			throw new IllegalArgumentException("name cannot be empty.");
		}

		if (grade < 0 || grade > 100) {
			throw new IllegalArgumentException("grade of out range");
		}

		this.name = name;
		this.grade = grade;
	}

	/**
	 * Returns the Student's grade
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the student's grade
	 */
	public int getGrade() {
		return this.grade;
	}

	/**
	 * Returns the student's name
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the student's name
	 */
	public String getName() {
		return this.name;
	}

	@Override
	public String toString() {
		return this.name + ": " + this.grade;
	}

}
